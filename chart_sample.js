function* dateGenerator(startYear, startMonth) {
    const date = new Date(startYear, startMonth);
    const now = Date.now();
    while (date < now) {
        yield `${date.getMonth() + 1}月${date.getDate()}日`;
        date.setDate(1 + date.getDate());
    }
}

function createChart(title, items, startYear, startMonth) {
    const canvas = document.getElementById('chart');
    canvas.style.borderRadius = '1rem';
    const ctx = canvas.getContext('2d');
    const labels = [...dateGenerator(startYear, startMonth)];
    // chart.jsはデフォルトでは背景色を設定できないので、プラグインを差し込む
    // 実処理はHTML5のcanvas
    Chart.pluginService.register({
        beforeDraw: function (chart, easing) {
            const ctx = chart.chart.ctx;
            ctx.save();
            const gradient = ctx.createLinearGradient(0, 0, 0, chart.chart.height);
            gradient.addColorStop(0, 'rgb(96, 96, 96)')
            gradient.addColorStop(1, 'rgb(16, 16, 16)')
            ctx.fillStyle = gradient;
            ctx.fillRect(0, 0, chart.chart.width, chart.chart.height);
            ctx.restore();
        }
    });
    Chart.defaults.global.defaultFontColor = 'rgb(140, 140, 140)';
    new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: '閲覧数',
                backgroundColor: '#dddf0d',
                borderColor: '#dddf0d',
                pointRadius: 0,
                borderWidth: 1,
                data: items,
                fill: false,
                lineTension: 0.1,
            }],
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: title,
                fontSize: 18,
                fontColor: 'rgb(255, 255, 255)',
            },
            scales: {
                xAxes: [{
                    type: 'time',
                    time: {
                        parser: 'MM月DD日',
                        tooltipFormat: "MM月DD日"
                    },
                    gridLines: {
                        lineWidth: 0,
                        zeroLineColor: 'rgb(153, 153, 153)',
                    }
                }],
                yAxes: [{
                    gridLines: {
                        color: 'rgba(255, 255, 255, 0.1)',
                        zeroLineColor: 'rgb(153, 153, 153)',
                    },
                }]
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            legend: {
                display: true,
                labels: {
                    boxWidth: 1,
                    fontColor: 'rgb(255, 255, 255)'
                }
            }
        }
    });
}

const title = $('#content h1').text();

$.getJSON('/api/tag_count/' + encodeURIComponent(title) + '?json=1', function (data) {
    var items = [];
    var now = _.date();

    for (var i = 0; i <= 30; i++) {
        var target = _.date().subtract({M: 30 - i}).date;
        var targetMonth = target.getMonth();

        if (data[i] == undefined) break;

        if (targetMonth == now.date.getMonth()) {
            var count = now.date.getDate();
        } else {
            var count = new Date(target.getFullYear(), target.getMonth() + 1, 0).getDate();
        }

        _(count).times(function (n) {
            if (data[i][n] == null) {
                items.push(0);
            } else {
                items.push(parseInt(data[i][n]));
            }
        })
    }
    createChart(title, items, now.subtract({M: 30}).date.getFullYear(), now.date.getMonth());
});
